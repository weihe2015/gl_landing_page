'use strict';

//Call this function when the page loads (the "ready" event)
$(document).ready(function() {
	validate.init();
	
	/* contact form */
	$('.cform_v2').on('submit', function(evt){
		validate.init();

		$.ajax({
			type: 'POST',
			url: 'php/form.php',
			contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
			//data: 'data='+JSON.stringify(formData),
			data : $(this).serialize(),
			success: function(data)
			{
				$('.cform_v2')[0].reset();
				$('#cform_message').fadeIn();
				$('#cform_v2').fadeOut();
				$('#contact img.loader').fadeOut('slow',function(){$(this).remove()});
				$('#cform_message').fadeIn();
				$('#cform #submit').removeAttr('disabled');
				setInterval(function(){
					$('#cform_message').fadeOut('slow');
					$('.cform_v2').fadeIn('slow');
				}, 8000);
			},
			error: function(xhr, exception){
				var msg = '';
				if (xhr.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (xhr.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (xhr.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + xhr.responseText;
				}
				console.log("Sorry, error happen in user request from: " + msg);

				$('.cform_v2')[0].reset();
				$('#cform_message').fadeIn();
				$('#cform_v2').fadeOut();
				$('#contact img.loader').fadeOut('slow',function(){$(this).remove()});
				$('#cform_message').fadeIn();
				$('#cform #submit').removeAttr('disabled');
				setInterval(function(){
					$('#cform_message').fadeOut('slow');
					$('.cform_v2').fadeIn('slow');
				}, 8000);
			}
		});
		
		evt.preventDefault();
	});

	$('#cform_v2-submit-btn').on('click', function(e){
		e.preventDefault();
		var name_flag = checkForm.checkEmpty($('#cform_v2 #name'));
		var email_flag = checkForm.checkEmail($('#cform_v2 #email'));
		var phone_flag = checkForm.checkNumber($('#cform_v2 #phone'));
		var messageTxt_flag = checkForm.checkEmpty($('#cform_v2 #messageTxt'));
		if(name_flag && email_flag && phone_flag && messageTxt_flag)
		{
			$('.cform_v2').submit();
		}
	});

	$('.home_btn').on('click', function(evt){
		evt.preventDefault();
		window.open('index.html');
		$('#cform_message').fadeOut();
		$('.cform_v2').fadeIn();
	});

	/* Request login */
	$('#account_form_request_v2').on("submit", function(e){
		e.preventDefault();

		$.ajax({
			type: 'POST',
			url: 'php/form.php',
			contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
			data : $(this).serialize(),
			success: function(data)
			{
				$('#account_message').show('slow');
				$('.account-modal-body').fadeOut();
				$('#newusermodal .modal-footer').fadeOut();
				$('#account_form_request_v2')[0].reset();
				setInterval(function(){
					$('#account_message').fadeOut('slow');
					$('.account-modal-body').fadeIn('slow');
					$('#newusermodal .modal-footer').fadeIn('slow');
				}, 8000);
			},
			error: function(xhr, exception){
				var msg = '';
				if (xhr.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (xhr.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (xhr.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + xhr.responseText;
				}
				console.log("Sorry, error happen in account form request: " + msg);
								
				$('#account_message').show('slow');
				$('.account-modal-body').fadeOut();
				$('#newusermodal .modal-footer').fadeOut();
				$('#account_form_request_v2')[0].reset();
				setInterval(function(){
					$('#account_message').fadeOut('slow');
					$('.account-modal-body').fadeIn('slow');
					$('#newusermodal .modal-footer').fadeIn('slow');
				}, 8000);

			}
		});

	});
	
	// Check account Request form data
	$('#submit_account_form').on('click',function(){

		// Check form data value.
		var name_flag = checkForm.checkEmpty($('#account_form_request_v2 #name'));
		var surname_flag = checkForm.checkEmpty($('#account_form_request_v2 #surname'));
		var email_flag = checkForm.checkEmail($('#account_form_request_v2 #email'));
		var contact_flag = checkForm.checkNumber($('#account_form_request_v2 #contact'));
		var department_flag = checkForm.checkEmpty($('#account_form_request_v2 #department'));
		var agree = checkForm.checkAgree($('#Account_Agree'));

		if(name_flag && surname_flag && email_flag && contact_flag && department_flag && agree)
		{
			$('#account_form_request_v2').submit();
		}
		
	});

	// Handle new account user modal home page click
	$('#newusermodal .index_home').click(function(e){
		e.preventDefault();
		$("#newusermodal .close").click();
		window.open('index.html');
	});

	$('#newusermodal .close').click(function(e){
		$('.account-modal-body').fadeIn();
		$('#account_message').fadeOut();
		$('#newusermodal .modal-footer').fadeIn();
	});


	/* wordfast request form */
	$('#wordfast_form_request_v2').on("submit", function(e){
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: 'php/form.php',
			contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
			data : $(this).serialize(),
			success: function(data)
			{
				$('#wordfast_message').show('slow');
				$('.wordfast-modal-body').fadeOut();
				$('#licensemodal .modal-footer').fadeOut();
				$('#wordfast_form_request_v2')[0].reset();
				setInterval(function(){
					$('#wordfast_message').fadeOut('slow');
					$('.wordfast-modal-body').fadeIn('slow');
					$('#licensemodal .modal-footer').fadeIn('slow');
				}, 8000);
			},
			error: function(xhr, exception){
				var msg = '';
				if (xhr.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (xhr.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (xhr.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + xhr.responseText;
				}
				console.log("Sorry, error happen in wordfast form request: " + msg);

				$('#wordfast_message').show('slow');
				$('.wordfast-modal-body').fadeOut();
				$('#licensemodal .modal-footer').fadeOut();
				$('#wordfast_form_request_v2')[0].reset();
				setInterval(function(){
					$('#wordfast_message').fadeOut('slow');
					$('.wordfast-modal-body').fadeIn('slow');
					$('#licensemodal .modal-footer').fadeIn('slow');
				}, 8000);
			}
		});
	});

	$('#submit_wordfast_form').on('click', function(e){

		// Check form data value.
		var name_flag = checkForm.checkEmpty($('#wordfast_form_request_v2 #name'));
		var surname_flag = checkForm.checkEmpty($('#wordfast_form_request_v2 #surname'));
		var email_flag = checkForm.checkEmail($('#wordfast_form_request_v2 #email'));
		var contact_flag = checkForm.checkNumber($('#wordfast_form_request_v2 #contact'));
		var license_flag = checkForm.checkEmpty($('#wordfast_form_request_v2 #license'));

		if(name_flag && surname_flag && email_flag && contact_flag && license_flag)
		{
			$('#wordfast_form_request_v2').submit();
		}

	});	

	// Handle License request modal home page click
	$('#licensemodal .index_home').click(function(e){
		e.preventDefault();
		$("#licensemodal .close").click();
		window.open('index.html');
	});

	$('#licensemodal .close').click(function(e){
		$('.wordfast-modal-body').fadeIn();
		$('#wordfast_message').fadeOut();
		$('#licensemodal .modal-footer').fadeIn();
	});

	/* Support request form */
	$('#support_form_request_v2').on("submit", function(e){
		e.preventDefault();

		$.ajax({
			type: 'POST',
			url: 'php/form.php',
			contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
			data : $(this).serialize(),
			success: function(data)
			{
				$('#support_message').show('slow');
				$('.support-modal-body').fadeOut();
				$('#supportmodal .modal-footer').fadeOut();
				$('#support_form_request_v2')[0].reset();
				if($('.upload-div').children().length > 0)
				{
					var listItems = $('.upload-div li');
					listItems.each(function(idx, li){
						$(li).remove();
					});
				}
				setInterval(function(){
					$('#support_message').fadeOut('slow');
					$('.support-modal-body').fadeIn('slow');
					$('#supportmodal .modal-footer').fadeIn('slow');
				}, 8000);
			},
			error: function(xhr, exception){
				var msg = '';
				if (xhr.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (xhr.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (xhr.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + xhr.responseText;
				}
				console.log("Sorry, error happen in support form request : " + msg);

				$('#support_message').show('slow');
				$('.support-modal-body').fadeOut();
				$('#supportmodal .modal-footer').fadeOut();
				$('#support_form_request_v2')[0].reset();
				if($('.upload-div').children().length > 0)
				{
					var listItems = $('.upload-div li');
					listItems.each(function(idx, li){
						$(li).remove();
					});
				}
				setInterval(function(){
					$('#support_message').fadeOut('slow');
					$('.support-modal-body').fadeIn('slow');
					$('#supportmodal .modal-footer').fadeIn('slow');
				}, 8000);
				
				// remove files by calling php/removeFiles.php
				$.ajax({
					type: 'POST',
					url: 'php/removeFiles.php',
					contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
					success: function(data)
					{
						console.log(data);
					}
				});
			}
		});

	});

	$('#support-request-form-submit').on('click', function(e)
	{
		// Check form data value.
		var name_flag = checkForm.checkEmpty($('#support_form_request_v2 #name'));
		var surname_flag = checkForm.checkEmpty($('#support_form_request_v2 #surname'));
		var username_flag = checkForm.checkEmpty($('#support_form_request_v2 #username'));
		var email_flag = checkForm.checkEmail($('#support_form_request_v2 #email'));
		var contact_flag = checkForm.checkNumber($('#support_form_request_v2 #contact'));
		var application_flag = checkForm.checkEmpty($('#support_form_request_v2 #application'));

		var application = $('#support_form_request_v2 #application').val();
		var description_flag = checkForm.checkEmpty($('#support_form_request_v2 #messageTxt'));

		if(application == 'PD')
		{
			var submission_ID_flag = checkForm.checkEmpty($('#support_form_request_v2 #subid'));

			if(name_flag && surname_flag && username_flag && email_flag && 
				contact_flag && application_flag && submission_ID_flag && description_flag)
			{
				$('#support_form_request_v2').submit();
			}
		}
		else
		{
			if(name_flag && surname_flag && username_flag && email_flag && 
				contact_flag && application_flag && description_flag)
			{
				$('#support_form_request_v2').submit();
			}
		}

	});

	// Handle Support request modal home page click
	$('#supportmodal .index_home').click(function(e){
		e.preventDefault();
		$("#supportmodal .close").click();
		window.open('index.html');
	});

	$('#supportmodal .close').click(function(e){
		$('.support-modal-body').fadeIn();
		$('#support_message').fadeOut();
		$('#supportmodal .modal-footer').fadeIn();
	});

	/* upload file button, disable modal from parent element */
	$('#screenshot_btn').click(function(e){
		e.stopPropagation();
		$('#upload-screenshot-hidden').click();
		$('.screen-div').addClass('uploaded-file');
		if($('.screen-div').parent().children('.error-message').length > 0)
		{
			$('.screen-div').parent().children('.error-message').remove();
			$('.screen-div').removeClass('error');
		}
	});

	$('#screenshot_upload_btn').on('click', function(e){
		e.stopPropagation();
		var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];

		if($('.screen-div').hasClass('uploaded-file'))
		{
			$('.screen-div').removeClass('error');
			$('.screen-div').parent().children('.error-message').remove();

			var file_data = $('#upload-screenshot-hidden').prop('files')[0];
			if(undefined == file_data)
			{
				$('.screen-div').addClass('error');
				$('.screen-div').removeClass('uploaded-file');
				$('.screen-div').after('<div class="error-message" style="color:red">&#10033;No File is chosen.</div>');
				return;
			}
			var file_size = file_data.size;

			var fileName = file_data.name;
			var file_type = fileName.substr(fileName.lastIndexOf('.'));
			if(_validFileExtensions.indexOf(file_type.toLowerCase()) != -1)
			{
				$('#upload-screenshot-form_ID').submit();
			}
			else if(file_size > Math.pow(10,7))
			{
				alert("Sorry, " + fileName + " is too large. We only accept file within 10MB.");	
			}
			else
			{
				alert("Sorry, " + fileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
			}
		}
		else
		{
			$('.screen-div').addClass('error');
			$('.screen-div').after('<div class="error-message" style="color:red">&#10033;No File is chosen.</div>');
		}
	});

	$('#upload-screenshot-form_ID').on('submit',function(e){
		e.preventDefault();
		var file_data = $('#upload-screenshot-hidden').prop('files')[0];
		var form_data = new FormData();

		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10){
			dd='0'+dd;
		} 
		if(mm<10){
			mm='0'+mm;
		} 
		var curr_date = mm+"-"+dd+"-"+yyyy+"_"+today.getHours()+"-"+today.getMinutes()+"-"+today.getSeconds();

		form_data.append('file', file_data);
		form_data.append('form', 'upload-screenshot-form');
		form_data.append('file_id', curr_date);

		$.ajax({
			type: 'POST',
			url: 'php/form.php',
			cache: false,
			dataType: 'text',
			processData: false, // Don't process the files
			contentType: false, // Set content type to false as jQuery
			data : form_data,
			success: function(data)
			{
				console.log(data);
				$('.upload-div').append('<li id=\"'+curr_date+'\"> ' + file_data.name + ' is uploaded.  </li>');
				var del_btn = $('<button onclick="deleteFile($(this))" style="font-weight: normal;">Delete File</button>');
				$('#'+curr_date).append(del_btn);
			},
			error: function(xhr, exception){
				var msg = '';
				if (xhr.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (xhr.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (xhr.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + xhr.responseText;
				}
				console.log("Sorry, error happen: " + msg);
			}
		});	

	});

});


var checkForm={
	checkEmpty:function($this)
	{
		if (!$this.val())
		{
			$this.addClass('error');
			if($this.parent().children('.error-message').length == 0)
			{
				$this.after('<div class="error-message" style="color:red">&#10033;The '+$this.attr('id')+' field is required.</div>');
			}
			return false;
		}
		else
		{
			$this.removeClass('error');
			$this.parent().children('.error-message').remove();
			return true;
		}
	},
	checkEmail:function($this){
		if (!$this.val())
		{
			$this.addClass('error');
			if($this.parent().children('.error-message').length == 0)
			{
				$this.after('<div class="error-message" style="color:red">&#10033;The '+$this.attr('id')+' field is required.</div>');
			}
			return false;
		}
		else if(!this.isEmail($this.val()))
		{
			$this.addClass('error');
			if($this.parent().children('.error-message').length == 0)
			{
				$this.after('<div class="error-message" style="color:red">&#10033;The email format is not correct.</div>');
			}
			else if($this.parent().children('.error-message').length == 1)
			{
				$this.parent().children('.error-message').remove();
				$this.after('<div class="error-message" style="color:red">&#10033;The email format is not correct.</div>');
			}
			return false;
		}
		else
		{
			$this.removeClass('error');
			$this.parent().children('.error-message').remove();
			return true;
		}
			
	},
	checkNumber:function($this){
		if(!$this.val())
		{
			$this.addClass('error');
			if($this.parent().children('.error-message').length == 0)
			{
				$this.after('<div class="error-message" style="color:red">&#10033;The '+$this.attr('id')+' field is required.</div>');
			}
			else if($this.parent().children('.error-message').length == 1)
			{
				$this.parent().children('.error-message').remove();
				$this.after('<div class="error-message" style="color:red">&#10033;The '+$this.attr('id')+' field is required.</div>');
			}
			return false;
		}
		else if(!this.isPhoneNumber($this.val()))
		{
			$this.addClass('error');
			if($this.parent().children('.error-message').length == 0)
			{
				$this.after('<div class="error-message" style="color:red">&#10033;The phone number format is not correct.</div>');
			}
			else if($this.parent().children('.error-message').length == 1)
			{
				$this.parent().children('.error-message').remove();
				$this.after('<div class="error-message" style="color:red">&#10033;The phone number format is not correct.</div>');
			}
		}
		else
		{
			$this.removeClass('error');
			$this.parent().children('.error-message').remove();
			return true;
		}
	},
	//To check phone is valid
	isPhoneNumber:function(number){
		var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;  
		return re.test(number);
	},
	//To check email is valid
	isEmail:function(email){
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	},
	checkAgree:function($this){
		if($this.val() == 'on' || $this.val() == 0 )
		{
			$this.parent().addClass('error');
			if($this.parent().parent().children('.error-message').length == 0)
			{
				$this.parent().parent().after('<div class="error-message" style="color:red">&#10033;The '+$this.attr('id')+' field is required.</div>');
			}
			return false;
		}
		else
		{
			$this.parent().removeClass('error');
			$('.Account_Agree-div .error-message').remove();
			return true;
		}
	}
};

var validate={
		init:function(){
			var $this=this;
			// validation will be done at focus out event
			$('#name, #messageTxt, #surname, #username').focusout(function() { 
				$this.checkEmpty($(this));
			});
			$('#phone, #contact').focusout(function(){
				$this.checkNumber($(this));
			});
			$('#email').focusout(function() {
				$this.checkEmail($(this));
			});
		},
		checkNumber:function($this){
			if(!$this.val())
			{
				$this.addClass('error');
				if($this.parent().children('.error-message').length == 0)
				{
					$this.after('<div class="error-message" style="color:red">&#10033;The '+$this.attr('id')+' field is required.</div>');
				}
				else if($this.parent().children('.error-message').length == 1)
				{
					$this.parent().children('.error-message').remove();
					$this.after('<div class="error-message" style="color:red">&#10033;The '+$this.attr('id')+' field is required.</div>');
				}
			}
			else if(!this.isPhoneNumber($this.val()))
			{
				$this.addClass('error');
				if($this.parent().children('.error-message').length == 0)
				{
					$this.after('<div class="error-message" style="color:red">&#10033;The phone number format is not correct.</div>');
				}
				else if($this.parent().children('.error-message').length == 1)
				{
					$this.parent().children('.error-message').remove();
					$this.after('<div class="error-message" style="color:red">&#10033;The phone number format is not correct.</div>');
				}
			}
			else
			{
				$this.removeClass('error');
				$this.parent().children('.error-message').remove();
			}
				
		},
		//To check phone is valid
		isPhoneNumber:function(number){
			var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;  
			return re.test(number);
		},
		//To check email is valid
		isEmail:function(email){
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		},
		checkEmpty:function($this){
			if (!$this.val())
			{
				$this.addClass('error');
				if($this.parent().children('.error-message').length == 0)
				{
					$this.after('<div class="error-message" style="color:red">&#10033;The '+$this.attr('id')+' field is required.</div>');
				}
			}
			else
			{
				$this.removeClass('error');
				$this.parent().children('.error-message').remove();
			}
				
		},
		checkEmail:function($this){
			if (!$this.val())
			{
				$this.addClass('error');
				if($this.parent().children('.error-message').length == 0)
				{
					$this.after('<div class="error-message" style="color:red">&#10033;The '+$this.attr('id')+' field is required.</div>');
				}
				else if($this.parent().children('.error-message').length == 1)
				{
					$this.parent().children('.error-message').remove();
					$this.after('<div class="error-message" style="color:red">&#10033;The '+$this.attr('id')+' field is required.</div>');
				}
			}
			else if(!this.isEmail($this.val()))
			{
				$this.addClass('error');
				if($this.parent().children('.error-message').length == 0)
				{
					$this.after('<div class="error-message" style="color:red">&#10033;The email format is not correct.</div>');
				}
				else if($this.parent().children('.error-message').length == 1)
				{
					$this.parent().children('.error-message').remove();
					$this.after('<div class="error-message" style="color:red">&#10033;The email format is not correct.</div>');
				}
			}
			else
			{
				$this.removeClass('error');
				$this.parent().children('.error-message').remove();
			}
		}
		
};

function checkCaptcha(){
	if ($('#captcha_input').val() == $('#hidCap').val()){
		$('#captcha_input').removeClass('error');
		$('.errCap').remove();
	}
	else{
		$('#captcha_input').addClass('error');
		var errtxt = $('#hidCap').attr('data-error');
		$('#captcha_input').after('<b class="errCap">'+errtxt+'</b>');
	}
}

function deleteFile($this)
{
	var file_id = $this.parent().attr('id');
	var form_data = new FormData();
	form_data.append('form', 'remove-screenshot');
	form_data.append('file_id', file_id);

	$.ajax({
		type: 'POST',
		url: 'php/form.php',
		cache: false,
		dataType: 'text',
		processData: false, // Don't process the files
		contentType: false, // Set content type to false as jQuery
		data : form_data,
		success: function(data)
		{
			$('.upload-div').append('<li class="removed-file-li">' + 'File: ' + data + ' has been deleted.' + '</li>');
			setInterval(function(){
				$('.removed-file-li').fadeOut('slow');
			}, 3000);
		},
		error: function(xhr, exception){
			var msg = '';
			if (xhr.status === 0) {
				msg = 'Not connect.\n Verify Network.';
			} else if (xhr.status == 404) {
				msg = 'Requested page not found. [404]';
			} else if (xhr.status == 500) {
				msg = 'Internal Server Error [500].';
			} else if (exception === 'timeout') {
				msg = 'Time out error.';
			} else if (exception === 'abort') {
				msg = 'Ajax request aborted.';
			} else {
				msg = 'Uncaught Error.\n' + xhr.responseText;
			}
			alert("Sorry, error happen: " + msg);
		}
	});
	$this.parent().remove();
}
