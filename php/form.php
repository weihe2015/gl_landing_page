<?php
require_once('phpMailer/class.phpmailer.php');
require_once('HelpUtils.php');

// Configuration that needs to change:
$to = "whe@translations.com";//<== update the email address
$from_email = "noreply@translations.com";

// Constant value, don't change.
$upload_folder = "/data1/opt/html/uploads";
//$upload_folder = "C:/xampp/htdocs/landingpage/uploads";

$mailer = new PHPMailer(); 
$helper = new HelperUtils();

date_default_timezone_set('America/New_York');
$mydate=date("Y-m-d H:i:s");

$form = $_POST['form'];

if(!isset($_POST['form']))
{
	//This page should not be accessed directly. Need to submit the form.
	header('Location: https://qa-tdc4.translations.com/index.html');
	echo "error; bad call";
	exit;
}

// Save request information into database;
$conn = $helper->connect();
// Check connection
if (!$conn) 
{
	echo("Connection failed: " . mysqli_connect_error());
}
$mysqli = $helper->createInstance();

/* Contact Form Handler */
if(strcmp($form, "contact-form") === 0)
{
	$name = $_POST['name'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$message = $_POST['message'];
	$client = $_POST['client'];
	$instance = $_POST['instance'];
	$env_report = $_POST['env_report'];

	if(IsInjected($email))
	{
	    echo "Bad email value!";
	    exit;
	}

	$email_subject = $client . " | Support Request:: New Support Message Request";
	$email_body = "Dear Support Team,\n \n".
		"A new support message was received:\n".
	  	"Message: " . $message . "\n" . 
		"\nThank you, \n" .
		"Translations.com\n\n" .    
		"====Auxiliary Info====\n" .
		"Name: " . $name . "\n" .
		"Email Address: " . $email . "\n" .
		"Main Contact Number: ". $phone . "\n" .
		"Client: " . $client . "\n" .
		"URL: " . $instance . "\n" .
		"Form: Contact Form\n"; 		

	$headers = "From: " . $from_email . "\r\n";
	$headers .= "Reply-To: " . $email . "\r\n";

	// Send the email!
	mail($to,$email_subject,$email_body,$headers);

	// Auto email:
	$auto_email_subject = "Automatic reply: " . $email_subject;
	$auto_email_body = "Hello " . $name . ",\n\n" .
			   "We have received your following request: \n\n" . $message . "\n\n" . 
			   "It has sent to our support team. " .
			   "You will receive the response within a few days.\n". 
			   "Please do not reply this email.\n\n" .
			   "Thank you,\nTranslations.com";

	$auto_header = "From: " . $from_email . "\r\n";
	$auto_header .= "Reply-To: " . $email . "\r\n";
	
	// Send auto reply email:
	mail($email,$auto_email_subject,$auto_email_body,$auto_header);
	$sql = "INSERT INTO landingpage.LANDING_REQUEST(REQUEST_TYPE, REQUEST_DATE, ".
		"REQUEST_ENV_REPORT, REQUEST_CLIENT, REQUEST_INSTANCE, REQUEST_USER_NAME, ".
		"REQUEST_USER_EMAIL, REQUEST_USER_PHONE, REQUEST_DESCRIPTION) VALUE (?,?,?,?,?,?,?,?,?);";

	if(!($stmt = $mysqli->prepare($sql)))
	{
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}

	if(!$stmt->bind_param("sssssssss",$form,$mydate,$env_report,$client,$instance,$name,$email,$phone,$message))
	{
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	if(!$stmt->execute())
	{
		echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
	}

	$stmt->close();
	mysqli_close($conn);
	    
}
/* New User Request Form Handler */
else if(strcmp($form, "new-user-request-form") === 0)
{
	$name = $_POST['name'];
	$surname=$_POST['surname'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$department = $_POST['department'];
	$client = $_POST['client'];
	$instance = $_POST['instance'];	
	$env_report = $_POST['env_report'];

	if(IsInjected($email))
	{
	    echo "Bad email value!";
	    exit;
	}

	$email_subject = $client .  " | Support Request: New Account Request";
	$email_body = "Dear Support Team,\n \n".
		"A new account creation request was received.\n".
		"\nThank you, \n" . 
		"Translations.com \n\n" .
		"====Auxiliary Info====\n" .
		"Name: " . $name . "\n" .
		"Surname: " . $surname . "\n" .
		"Email Address: " . $email . "\n" .
		"Main Contact Number: ". $phone . "\n" .
		"Department: " . $department . "\n" .
		"Client: " . $client . "\n" .
		"URL: " . $instance . "\n" .
	 	"Form: New User Form";	   
 
	$headers = "From: " . $from_email . "\r\n";
	$headers .= "Reply-To: " . $email . "\r\n";

	// Send the email!
	mail($to,$email_subject,$email_body,$headers);

	// Auto email:
	$auto_email_subject = "Automatic reply: " . $email_subject;
	$auto_email_body = "Hello " . $name . " " . $surname . ",\n\n" .
			   "We have received your following new account creation request. " . 
			   "It has sent to our support team. \n" .
			   "You will receive the response within a few days. ". 
			   "Please do not reply this email.\n\n" .
			   "Thank you,\nTranslations.com";

	$auto_header = "From: " . $from_email . "\r\n";
	$auto_header .= "Reply-To: " . $email . "\r\n";
	
	// Send auto reply email:
	mail($email,$auto_email_subject,$auto_email_body,$auto_header);
	
	$sql = "INSERT INTO landingpage.LANDING_REQUEST(REQUEST_TYPE, REQUEST_DATE, ".
		"REQUEST_ENV_REPORT, REQUEST_CLIENT, REQUEST_INSTANCE, REQUEST_USER_NAME, REQUEST_USER_SURNAME, REQUEST_USER_EMAIL, ".
		"REQUEST_USER_PHONE, REQUEST_USER_DEPARTMENT) VALUE (?,?,?,?,?,?,?,?,?,?);";

	if(!($stmt = $mysqli->prepare($sql)))
	{
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}

	if(!$stmt->bind_param("ssssssssss",$form,$mydate,$env_report,$client,$instance,$name,$surname,$email,$phone,$department))
	{
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	if(!$stmt->execute())
	{
		echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
	}

	$stmt->close();
	mysqli_close($conn);
}
else if(strcmp($form, "wordfast-pro-request-form") === 0)
{
	$name = $_POST['name'];
	$surname=$_POST['surname'];
	
	$code = "NULL";
	if(isset($_POST['code']) && strlen($_POST['code']) > 0)
	{
		$code = "Code: " . $_POST['code']. "\n";
	}

	$company = "";
	if(isset($_POST['company']) && strlen($_POST['company']) > 0)
	{		
		$company = "Vendor Company: " . $_POST['company'] . "\n";
	}

	$comment = "";
	if(isset($_POST['comment']) && strlen($_POST['comment']) > 0) 
	{
		$comment = "Comment: " . $_POST['comment'] . "\n";
	}

	$email=$_POST['email'];
	$phone = $_POST['phone'];
	$license = $_POST['license'];
	$client = $_POST['client'];
	$instance = $_POST['instance'];
	$env_report = $_POST['env_report'];

	$email_subject = $client . " | Support Request:: Wordfast Pro Request";
	
	$email_body = "Dear Support Team,\n \n".
		"A new wordfast pro request is received.\n".
		"\nThank you, \n" . 
		"Translations.com \n\n" . 
		"====Auxiliary Info====\n" . 
		"Name: " . $name . "\n" .
		"Surname: " . $surname . "\n" .
		"Email Address: " . $email . "\n" .
		$code . 
		$company . 
		"Main Contact Number: ". $phone . "\n" .
		"License Type: " . $license . "\n" .
		$comment .
		"Form: WordFast Request Form";

	$headers = "From: " . $from_email . "\r\n";
	$headers .= "Reply-To: " . $email . "\r\n";

	// Send the email!
	mail($to,$email_subject,$email_body,$headers);

	// Auto email:
	$auto_email_subject = "Automatic reply: " . $email_subject;
	$auto_email_body = "Hello " . $name . " " . $surname . ",\n\n" .
			   "We have received your following WordFast Pro request. " . 
			   "It has sent to our support team. \n" .
			   "You will receive the response within a few days. ". 
			   "Please do not reply this email.\n\n" .
			   "Thank you,\nTranslations.com";

	$auto_header = "From: " . $from_email . "\r\n";
	$auto_header .= "Reply-To: " . $email . "\r\n";
	
	// Send auto reply email:
	mail($email,$auto_email_subject,$auto_email_body,$auto_header);

	if(strlen($_POST['code']) > 0)
	{
		$code = $_POST['code'];
	}
	if(strlen($_POST['company']) > 0)
	{
		$company = $_POST['company'];
	}

	$sql = "INSERT INTO landingpage.LANDING_REQUEST(REQUEST_TYPE, REQUEST_DATE, ".
		"REQUEST_ENV_REPORT, REQUEST_CLIENT, REQUEST_INSTANCE, REQUEST_USER_NAME, REQUEST_USER_SURNAME, REQUEST_VENDOR_COMPANY, ".
		"REQUEST_USER_CODE, REQUEST_USER_EMAIL, REQUEST_USER_PHONE, REQUEST_WF_LICENSE) VALUE (?,?,?,?,?,?,?,?,?,?,?,?);";

	if(!($stmt = $mysqli->prepare($sql)))
	{
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}

	if(!$stmt->bind_param("ssssssssssss",$form,$mydate,$env_report,$client,$instance,$name,$surname,$company,$code,$email,$phone,$license))
	{
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	if(!$stmt->execute())
	{
		echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
	}

	$stmt->close();
	mysqli_close($conn);

}
// Support request Form
else if(strcmp($form, "support-request-form") === 0)
{
	$os = PHP_OS;
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
	{
		$upload_folder = str_replace("/", "\\", $upload_folder);
	}

	// Scan upload folder for attachement.
	$iter = new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator($upload_folder, RecursiveDirectoryIterator::SKIP_DOTS),
		RecursiveIteratorIterator::SELF_FIRST,
		RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied" 
		);
	
	$paths = array($upload_folder);
	$upload_files = Array();

	foreach ($iter as $path => $dir)
	{
		if($dir->isDir())
		{
			$paths[] = $path;
		}
		else
		{
			array_push($upload_files, $dir->getPathname());
		}
	}

	// prepare form data to email
	$name = $_POST['name'];
	$surname=$_POST['surname'];
	$username=$_POST['username'];
	$email=$_POST['email'];
	$phone = $_POST['phone'];
	$application = $_POST['application'];
	$message = $_POST['message'];
	$client = $_POST['client'];
	$instance = $_POST['instance'];
	$env_report = $_POST['env_report'];

	$email_subject = $client . " | " . " Application: " . $application . " | Support Request:: General Support Request";
	$email_body = "";

	if(IsInjected($email))
	{
	    echo "Bad email value!";
	    exit;
	}

	if(strcmp($application, "PD") === 0)
	{
		$subid = $_POST['subid'];
		$email_subject = $client . " | " . " Application: " . $application . " -SubId " . $subid . " | Support Request:: PD Support Request ";

		$email_body = "Dear Support Team,\n \n".
			"A new support request was recieved: \n".
			"Application: " . $application . "\n" .
			"Submission ID: " . $subid . "\n" .
			"Message: " . $message . 
			"\n \nThank you, \n" .
			"Translations.com\n\n" .
			"====Auxiliary Info====\n" .
			"Name: " . $name . "\n" .
			"Surname: " . $surname . "\n" .
			"Email Address: " . $email . "\n" .
			"Main Contact Number: ". $phone . "\n" .
			"Client: " . $client . "\n" .
			"Instance: " . $instance . "\n" . 
			"Form: Support Request Form ";
	}
	else
	{
		$email_body = "Dear Support Team,\n \n".
			"A new support request was recieved: \n".
			"Application: " . $application . "\n" .
			"Message: " . $message . 
			"\n \nThank you, \n" .
			"Translations.com\n\n" .
			"====Auxiliary Info====" .
			"Name: " . $name . "\n" .
			"Surname: " . $surname . "\n" .
			"Email Address: " . $email . "\n" .
			"Main Contact Number: ". $phone . "\n" .
			"Client: " . $client . "\n" .
			"Instance: " . $instance . "\n" . 
			"Form: Support Request Form ";
	}
	    
	if(sizeof($upload_files) > 0)
	{
	     $mailer->From = $from_email;
	     $mailer->FromName = "GL Team";
	     $mailer->addAddress($to, $name);

	     $mailer->isHTML(false);
	     
	     $mailer->Subject = $email_subject;
	     $mailer->Body = $email_body;
		
	     foreach ($upload_files as $upload_file)
	     {
	        $fileName = basename($upload_file);
	        $mailer->addStringAttachment(file_get_contents($upload_file), $fileName);
	     }
	    		
	     if(!$mailer->send())
	     {
			echo "Mailer Error: " . $mailer->ErrorInfo;
	     }
	     else
	     {
			echo "Message has been sent successfully";
	     }
	}
	else
	{
		// From email address and name
		$mailer->From = $from_email;
		$mailer->FromName = "GL Team";

		// To address and name
		$mailer->addAddress($to, "Support Team");
		$mailer->addReplyTo($email, "Reply");
		
		// Send HTML or Plain Text email
		$mailer->isHTML(false);

		$mailer->Subject = $email_subject;
		$mailer->Body = $email_body;
		
		if(!$mailer->send())
		{
			echo "Mailer Error: " . $mailer->ErrorInfo;
		}
		else
		{
			echo "Message has been sent successfully";
		}
		
	}	
	
	// Auto email:
	$auto_email_subject = "Automatic reply: " . $email_subject;
	$auto_email_body = "Hello " . $name . " " . $surname . ",\n\n" .
			   "We have received your following Support Request. \n" . 
			   "It has sent to our support team. " .
			   "You will receive the response within a few days.\n". 
			   "Please do not reply this email.\n\n" .
			   "Thank you,\nTranslations.com";

	$auto_header = "From: " . $from_email . "\r\n";
	$auto_header .= "Reply-To: " . $email . "\r\n";

	// Send auto reply email:
	mail($email,$auto_email_subject,$auto_email_body,$auto_header);

	foreach ($upload_files as $upload_file)
	{
		$fp = fopen($upload_file, 'r');
		$fileName = basename($upload_file);
		$fileSize = filesize($upload_file);
		$fileType = filetype($upload_file);
		$content = fread($fp, $fileSize);
		$content = addslashes($content);
		fclose($fp);

		$sub_sql = "INSERT INTO landingpage.LANDING_REQUEST_SCREENSHOT (REQUEST_SCREENSHOT_NAME,".
			" REQUEST_SCREENSHOT_SIZE, REQUEST_SCREENSHOT_TYPE, REQUEST_SCREENSHOT_CONTENT) VALUES".
			" ('".$fileName."', '".$fileSize."', '".$fileType."', '".$content."');";
		$conn->query($sub_sql);

		$screenshot_id = mysqli_insert_id($conn);

		if(strcmp($application, "PD") === 0)
		{
			$sql = "INSERT INTO landingpage.LANDING_REQUEST(REQUEST_TYPE, REQUEST_DATE, ".
				"REQUEST_ENV_REPORT, REQUEST_CLIENT, REQUEST_INSTANCE, REQUEST_USER_NAME, REQUEST_USER_SURNAME, REQUEST_USER_GLUSER, ".
				"REQUEST_USER_EMAIL, REQUEST_USER_PHONE, REQUEST_GL_APPLICATION, REQUEST_GL_SUBID, REQUEST_DESCRIPTION, REQUEST_SCREENSHOT_ID) ".
				"VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

			if(!($stmt = $mysqli->prepare($sql)))
			{
				echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
			}

			if(!$stmt->bind_param("sssssssssssisi",$form,$mydate,$env_report,$client,$instance,$name,$surname,$username,$email,$phone,$application,$subid,$message,$screenshot_id))
			{
				echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
			}

		}
		else
		{
			$sql = "INSERT INTO landingpage.LANDING_REQUEST(REQUEST_TYPE, REQUEST_DATE, ".
				"REQUEST_ENV_REPORT, REQUEST_CLIENT, REQUEST_INSTANCE, REQUEST_USER_NAME, REQUEST_USER_SURNAME, REQUEST_USER_GLUSER, ".
				"REQUEST_USER_EMAIL, REQUEST_USER_PHONE, REQUEST_GL_APPLICATION, REQUEST_DESCRIPTION, REQUEST_SCREENSHOT_ID) ".
				"VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

			if(!($stmt = $mysqli->prepare($sql)))
			{
				echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
			}

			if(!$stmt->bind_param("ssssssssssssi",$form,$mydate,$env_report,$client,$instance,$name,$surname,$username,$email,$phone,$application,$message,$screenshot_id))
			{
				echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
			}

		}
		
		if(!$stmt->execute())
		{
			echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
		}
		$stmt->close();

	}

	if(sizeof($upload_files) == 0)
	{
		if(strcmp($application, "PD") === 0)
		{
			$sql = "INSERT INTO landingpage.LANDING_REQUEST(REQUEST_TYPE, REQUEST_DATE, ".
				"REQUEST_ENV_REPORT, REQUEST_CLIENT, REQUEST_INSTANCE, REQUEST_USER_NAME, REQUEST_USER_SURNAME, REQUEST_USER_GLUSER, ".
				"REQUEST_USER_EMAIL, REQUEST_USER_PHONE, REQUEST_GL_APPLICATION, REQUEST_GL_SUBID, REQUEST_DESCRIPTION) ".
				"VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?);";

			if(!($stmt = $mysqli->prepare($sql)))
			{
				echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
			}

			if(!$stmt->bind_param("sssssssssssis",$form,$mydate,$env_report,$client,$instance,$name,$surname,$username,$email,$phone,$application,$subid,$message))
			{
				echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
			}
		}
		else
		{
			$sql = "INSERT INTO landingpage.LANDING_REQUEST(REQUEST_TYPE, REQUEST_DATE, ".
					"REQUEST_ENV_REPORT, REQUEST_CLIENT, REQUEST_INSTANCE, REQUEST_USER_NAME, REQUEST_USER_SURNAME, REQUEST_USER_GLUSER, ".
					"REQUEST_USER_EMAIL, REQUEST_USER_PHONE, REQUEST_GL_APPLICATION, REQUEST_DESCRIPTION) ".
					"VALUE (?,?,?,?,?,?,?,?,?,?,?,?);";

			if(!($stmt = $mysqli->prepare($sql)))
			{
				echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
			}

			if(!$stmt->bind_param("ssssssssssss",$form,$mydate,$env_report,$client,$instance,$name,$surname,$username,$email,$phone,$application,$message))
			{
				echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
			}
		}

		if(!$stmt->execute())
		{
			echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
		}
		$stmt->close();
	}

	mysqli_close($conn);

	// remove all upload file for next time.
	$di = new RecursiveDirectoryIterator($upload_folder, FilesystemIterator::SKIP_DOTS);
	$ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
	print_r($ri);
	foreach ( $ri as $file ) {
		echo "deleting: " . $file;
		$file->isDir() ?  rmdir($file) : unlink($file);
	}

}
else if(strcmp($form, "upload-screenshot-form") === 0)
{

	//upload the file
	$folder_date = $_POST['file_id'];
        
	$finfo = new finfo(FILEINFO_MIME_TYPE);
	$fileContent = file_get_contents($_FILES['file']['tmp_name']);
	$mimeType = $finfo->buffer($fileContent);
	print_r ($_FILES['file']);
	$os = PHP_OS;
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
	{
		$upload_folder = str_replace("/", "\\", $upload_folder);
		$target_folder = $upload_folder . "\\" . $folder_date;
		mkdir($target_folder, 0777, true);
		
		// Check if uploaded file is image file
		$size = getimagesize($_FILES['file']['tmp_name']);
	        if($size)
		{
			move_uploaded_file($_FILES["file"]["tmp_name"], $upload_folder . "\\" . $folder_date . "\\" . $_FILES["file"]["name"]);
		}
	}
	else
	{
		$target_folder = $upload_folder . "/" . $folder_date;
		mkdir($target_folder, 0777, true);

		// Check if uploaded file is image file
		$size = getimagesize($_FILES['file']['tmp_name']);
		if($size)
		{
			rename($_FILES["file"]["tmp_name"], $target_folder."/".$_FILES["file"]["name"]);
			move_uploaded_file($_FILES["file"]["tmp_name"], $upload_folder . "/" . $folder_date . "/" . $_FILES["file"]["name"]);
		}
	}
}
/* Remove uploaded file */
else if(strcmp($form, "remove-screenshot") === 0)
{
	$folder_date = $_POST['file_id'];

	$delete_folder = $upload_folder . "/" . $folder_date;
	$fileName = scandir($delete_folder);
	if(isset($fileName))
	{
	echo $fileName[2];

	// remove all upload file for next time.
	$di = new RecursiveDirectoryIterator($delete_folder, FilesystemIterator::SKIP_DOTS);
	$ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
	foreach ( $ri as $file ) {
		//echo "Not deleting: ";
		$file->isDir() ?  rmdir($file) : unlink($file);
	}
	rmdir ($delete_folder);
	}
}


// Function to validate against any email injection attempts
function IsInjected($str)
{
  $injections = array('(\n+)',
              '(\r+)',
              '(\t+)',
              '(%0A+)',
              '(%0D+)',
              '(%08+)',
              '(%09+)'
              );
  $inject = join('|', $injections);
  $inject = "/$inject/i";
  if(preg_match($inject,$str))
    {
    return true;
  }
  else
    {
    return false;
  }
}

?>
