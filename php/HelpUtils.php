<?php

class HelperUtils {

	private $servername = "localhost";
	private $database = "landingpage";
	private $username = "pd4";
	private $password = "n4il1t";

	public function connect()
	{
		// Create connection
		$conn = mysqli_connect($this->servername, $this->username, $this->password, $this->database);
		return $conn;
	}

	public function createInstance()
	{
		$mysqli = new mysqli($this->servername, $this->username, $this->password, $this->database);

		$screenSQL = "CREATE TABLE IF NOT EXISTS `LANDING_REQUEST_SCREENSHOT`( " .
				   	"`REQUEST_SCREENSHOT_ID` BIGINT(20) NOT NULL AUTO_INCREMENT,".
					"`REQUEST_SCREENSHOT_NAME` VARCHAR(255) NOT NULL, ".
					"`REQUEST_SCREENSHOT_SIZE` BIGINT(20) NOT NULL,".
					"`REQUEST_SCREENSHOT_TYPE` VARCHAR(255) NOT NULL,".
					"`REQUEST_SCREENSHOT_CONTENT` MEDIUMBLOB NOT NULL,". 
					"PRIMARY KEY (`REQUEST_SCREENSHOT_ID`)) ".
					"COLLATE='utf8mb4_unicode_ci' ".
					"ENGINE=InnoDB ".
					"AUTO_INCREMENT=1;";

		$requestSQL = "CREATE TABLE IF NOT EXISTS `LANDING_REQUEST` ( ".
					"`REQUEST_ID` BIGINT(20) NOT NULL AUTO_INCREMENT,".
					"`REQUEST_TYPE` VARCHAR(255) NOT NULL,".
					"`REQUEST_DATE` VARCHAR(255) NOT NULL,".
					"`REQUEST_ENV_REPORT` TEXT  NOT NULL,".
					"`REQUEST_CLIENT` VARCHAR(255) NOT NULL,".
					"`REQUEST_INSTANCE` TEXT NOT NULL,".
					"`REQUEST_USER_NAME` VARCHAR(255) NOT NULL,".
					"`REQUEST_USER_SURNAME` VARCHAR(255) NULL DEFAULT NULL,".
					"`REQUEST_USER_EMAIL` VARCHAR(50) NOT NULL,".
					"`REQUEST_USER_PHONE` VARCHAR(20) NOT NULL,".
					"`REQUEST_USER_DEPARTMENT` VARCHAR(20) NULL DEFAULT NULL,".
					"`REQUEST_USER_CODE` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',".
					"`REQUEST_WF_LICENSE` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',".
					"`REQUEST_VENDOR_COMPANY` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',".
					"`REQUEST_USER_GLUSER` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',".
					"`REQUEST_GL_APPLICATION` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',".
					"`REQUEST_GL_SUBID` BIGINT(20) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',".
					"`REQUEST_DESCRIPTION` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',".
					"`REQUEST_TEXT` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',".
					"`REQUEST_SCREENSHOT_ID` BIGINT(20) NULL DEFAULT NULL,".
					"PRIMARY KEY (REQUEST_ID),".
					"FOREIGN KEY (`REQUEST_SCREENSHOT_ID`) REFERENCES `LANDING_REQUEST_SCREENSHOT` (`REQUEST_SCREENSHOT_ID`)) ".
					"COLLATE='utf8mb4_unicode_ci' ".
					"ENGINE=InnoDB ".
					"AUTO_INCREMENT=1;";

		if(!$mysqli->query($screenSQL))
		{
			echo "Table creation failed: (" . $mysqli->errno . ") " . $mysqli->error;
		}
		if(!$mysqli->query($requestSQL))
		{
			echo "Table creation failed: (" . $mysqli->errno . ") " . $mysqli->error;
		}
		return $mysqli;
	}

}

?>
